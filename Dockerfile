FROM debian

RUN apt-get update && apt-get install -y apache2 &&\
    mkdir /var/www/mynewsite 

COPY files/html/index.html  /var/www/mynewsite

COPY files/apache/mysite.com.conf /etc/apache2/sites-available

RUN a2ensite mysite.com &&\
    service apache2 reload
